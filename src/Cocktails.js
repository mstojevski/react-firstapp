import React, { Component } from 'react';
import axios from 'axios';
import Button from './Button';

const ENDPOINT =
  'https://www.thecocktaildb.com/api/json/v1/1/filter.php?a=Alcoholic';

class Cocktails extends Component {
  state = {
    isLoading: false,
    cocktails: null,
  };

  componentDidMount() {
    axios.get(ENDPOINT).then(cocktails =>
      this.setState(prevState => {
        return {
          cocktails: cocktails.data.drinks,
          isLoading: true,
        };
      })
    );
  }

  changeLoadingState = () => {
    this.setState(prevState => {
      return {
        isLoading: !prevState,
      };
    });
  };

  render() {
    console.log(this.state);
    return (
      <div className="Site-content">
        {this.state.cocktails &&
          this.state.isLoading &&
          this.state.cocktails.slice(1, 10).map(cocktail => {
            return (
              <div key={cocktail.idDrink}>
                <img
                  src={cocktail.strDrinkThumb}
                  alt={cocktail.strDrink}
                  width="250"
                />
                <h3>{cocktail.strDrink}</h3>
              </div>
            );
          })}
        <Button onButtonClick={this.changeLoadingState} />
      </div>
    );
  }
}

export default Cocktails;
