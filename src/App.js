import React, { Component } from 'react';
import './App.css';
import Header from './Header';
import Footer from './Footer';
import Cocktails from './Cocktails';

class App extends Component {
  state = {
    author: "Micko"
  }
  render() {
    return (
      <div className="App">
        <Header title="drive responsibly"></Header>
        <Cocktails></Cocktails>
        <Footer copyright="Created By" author={this.state.author}></Footer>
      </div>
    );
  }
}

export default App;
