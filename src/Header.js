import React from 'react';

const Header = (props) => {
  return (
    <div className="Header">
      {props.title.toUpperCase()}
    </div>
  );
};

export default Header;