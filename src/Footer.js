import React from 'react';

const Footer = (props) => {
  return (
    <footer className="Footer">
      {props.copyright} {props.author}

    </footer>
  );
};

export default Footer;